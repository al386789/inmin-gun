using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FamilyFriendlyGame
{
    private static bool familyFriendly = true;

    public static void CambiarFamilyFriendly(bool estado)
    {
        familyFriendly = estado;
        Debug.Log("FamilyFriendly: " + familyFriendly);
    }

    public static bool EstadoFamilyFriendly()
    {
        return familyFriendly;
    }

}
