using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{

    public GameObject botones;

    public Animator animator;

    //private AudioSource audioSrc;
    private float musicVolume = 1f; 

    void Start()
    {
        //audioSrc = GetComponent<AudioSource>();
    }

    private void Update()
    {
        //audioSrc.volume = musicVolume;
    }
    public void Iniciar()
    {
        StartCoroutine(FadeOutCourrutine());
        
        
    }

    public void Salir()
    {
        Application.Quit();
    }

    public void SetVolume(float vol)
    {
        musicVolume = vol;
    }

    public void Instrucciones()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(2);
    }

    public void FadeOut()
    {
        animator.Play("FadeOut");
    }

    IEnumerator FadeOutCourrutine()
    {
        Invoke("FadeOut", 1);
        for (int i = 0; i < botones.transform.childCount; i++)
        {
            botones.transform.GetChild(i).gameObject.SetActive(false);
        }
        yield return new WaitForSeconds(4);
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);


    }
}
