using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnBala : MonoBehaviour
{
    public void CrearBala(GameObject bala, GameObject objetivo, int da�o, bool doble)
    {
        if (doble)
        {
            Vector3 posicion = new Vector3();
            if (transform.position.y > objetivo.transform.position.y)
                posicion = new Vector3(transform.position.x + 0.2f, transform.position.y - 0.8f, transform.position.z);
            else
                posicion = new Vector3(transform.position.x + 0.2f, transform.position.y + 0.8f, transform.position.z);
            GameObject nuevaBala = Instantiate(bala, posicion, transform.rotation);
            nuevaBala.GetComponent<movimientoBala>().objectivo = objetivo;
            nuevaBala.GetComponent<movimientoBala>().da�o = da�o;
        }
        else
        {
            GameObject nuevaBala = Instantiate(bala, transform.position, transform.rotation);
            nuevaBala.GetComponent<movimientoBala>().objectivo = objetivo;
            nuevaBala.GetComponent<movimientoBala>().da�o = da�o;
        }

    }
}
