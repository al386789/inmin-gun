using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour
{
    public GameObject enemigo;
    public AudioClip sapo;
    public AudioClip fondo;
    public AudioClip fantasma;
    public AudioClip ara�a;
    public AudioClip jamas;
    public AudioClip payaso;

    AudioSource sonido;


    // Start is called before the first frame update
    void Start()
    {
        sonido = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (//sapo)
        {
            sonido.clip = sapo;
        }
        else if (//fantasma)
        {
            sonido.clip = fantasma;
        }
        else if (//ara�a)
        {
            sonido.clip = ara�a;
        }
        else if (//jamas)
        {
            sonido.clip = jamas;
        }
        else if (//payaso)
        {
            sonido.clip = payaso;
        }
        else
        {
            sonido.clip = fondo;
        }
    }
}
