using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    public GameObject start;
    public GameObject instrucciones;
    public GameObject opciones;
    public GameObject volumen;
    public GameObject atras;
    public GameObject salir;
    public GameObject famillyFriendly;
    public GameObject NSFW;

    public void ActivarOpciones()
    {
        start.SetActive(false);
        instrucciones.SetActive(true);
        opciones.SetActive(false);
        volumen.SetActive(true);
        atras.SetActive(true);
        salir.SetActive(false);
        famillyFriendly.SetActive(false);
        NSFW.SetActive(false);
    }

    public void DesactivarOpciones()
    {
        start.SetActive(true);
        instrucciones.SetActive(false);
        opciones.SetActive(true);
        volumen.SetActive(false);
        atras.SetActive(false);
        salir.SetActive(true);
        famillyFriendly.SetActive(true);
        NSFW.SetActive(true);
    }

    public void setFamilyFriendly(bool value)
    {
        Debug.Log("Estado que llega: " + value);
        FamilyFriendlyGame.CambiarFamilyFriendly(value);
    }
}
