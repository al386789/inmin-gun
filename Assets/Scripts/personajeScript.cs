using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class personajeScript : MonoBehaviour
{

    public float velocidad = 6f;


    public GameObject ataque;
    public GameObject mira;
    public int da�o;
    public bool balaDoble = false;

    private float time;
    private Rigidbody2D rb2d;
    private float velocidadIni;
    private int da�oIni;

    private Animator anim;
    


    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        velocidadIni = velocidad;
        da�oIni = da�o;
    }

    // Update is called once per frame
    void Update()
    {
        var dir = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position);
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        anim.SetFloat("derecha", rb2d.velocity.x);

        if (angle > 315f && angle<=45f)
        {
            //transform.localScale = new Vector3(1f, 0, 1f);
        }

        if (angle > 180f && angle <= 225f)
        {
            //transform.localScale = new Vector3(-1f, 0, 1f);
        }

        if (angle > 90f && angle <= 135f)
        {
            //transform.localScale = new Vector3(0, 1f, 1f);
        }

        if (angle > 270f && angle <= 315f)
        {
            //transform.localScale = new Vector3(0, -1f, 1f);
        }
    }



    private void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        rb2d.transform.position += Vector3.right * velocidad * h * Time.deltaTime;
        rb2d.transform.position += Vector3.up * velocidad * v * Time.deltaTime;

        time += Time.deltaTime;

        if (Input.GetMouseButton(0) && time>=0.5)
        {
            playerAttack();
            time = 0;
        }
    }

    
    private void playerAttack()
    {
        GetComponentInChildren<spawnBala>().CrearBala(ataque, mira, da�o, false);
        if (balaDoble)
            GetComponentInChildren<spawnBala>().CrearBala(ataque, mira, da�o, true);
    }

    public void resetVariables(int type)
    {
        switch(type)
        {
            case 0: velocidad = velocidadIni; break;
            case 1: da�o = da�oIni; break;
        }
        
    }
}