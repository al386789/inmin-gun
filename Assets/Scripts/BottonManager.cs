using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottonManager : MonoBehaviour
{
    public GameObject puerta;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Contacto");
        if(collision.gameObject.tag == "Player")
        {
            puerta.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            puerta.gameObject.GetComponent<SpriteRenderer>().enabled = true;
            Destroy(gameObject);
        }
    }
}
