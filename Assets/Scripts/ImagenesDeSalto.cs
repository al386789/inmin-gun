using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImagenesDeSalto : MonoBehaviour
{
    public GameObject imagen;
    private float timer = 6.0f;
    private float time = 0;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "imagenTrigo")
        {
            Debug.Log("ImagenTrigo");
            imagen.SetActive(true);
        }
    }

    public void reiniciarImagen()
    {
        imagen.SetActive(false);
        gameObject.SetActive(false);
    }

    private void Update()
    {
        time += Time.deltaTime;
        if (time == timer)
        {
            reiniciarImagen();
            time = 0;
        }
    }

    
}
