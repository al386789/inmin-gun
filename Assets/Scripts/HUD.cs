using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUD : MonoBehaviour
{
    public int vidaCorazones;
    // Start is called before the first frame update
    void Start()
    {
        CambioVida(4);
        
    }

    // Update is called once per frame
    void Update()
    {
        CambioVida(vidaCorazones);
    }

    public void CambioVida(int pos)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.activeSelf)
                transform.GetChild(i).gameObject.SetActive(false);
        }
        transform.GetChild(pos).gameObject.SetActive(true);
    }
}
