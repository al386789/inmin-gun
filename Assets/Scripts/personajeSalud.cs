using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class personajeSalud : MonoBehaviour
{
    public int maxVida = 5;
    public int actualVida;

    public HUD vida_canvas;

    public bool inmortal = false;
    public float tiempoInmortal = 1.0f;

    private void Start()
    {
        actualVida = maxVida;
        vida_canvas.vidaCorazones = actualVida;
    }

    private void Update()
    {
        if (actualVida <= 0)
            Moriciones();
    }

    public void QuitarVida(int da�o)
    {
        if (inmortal) return;

        actualVida -= da�o;
        StartCoroutine(TiempoInmortal());

        vida_canvas.CambioVida(actualVida);
    }

    public void RecuperarVida(int vida)
    {
        actualVida += vida;
    }

    public void Moriciones()
    {
        Destroy(this.gameObject);
    }

    public void resetPowerUp()
    {
        inmortal = false;
    }

    IEnumerator TiempoInmortal()
    {
        inmortal = true;
        yield return new WaitForSeconds(tiempoInmortal);
        inmortal = false;
    }
}
