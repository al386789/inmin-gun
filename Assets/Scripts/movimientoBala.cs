using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimientoBala : MonoBehaviour
{
    public GameObject objectivo;
    public int da�o;
    public float velocidad = 6f;

    bool choque = false;
    Vector3 firstPos;

    private void Start()
    {
        firstPos = objectivo.transform.position;
    }
    // Update is called once per frame
    void Update()
    {
        if(!choque)
            moverBala();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Choque");
        if (collision.gameObject.tag == "Player")
        {
            choque = true;
            collision.gameObject.GetComponent<personajeSalud>().QuitarVida(da�o);
        }

        if(collision.gameObject.tag == "Enemigo")
        {
            choque = true;
            collision.gameObject.GetComponent<enemiesMovement>().quitarVida();
        }
        Destroy(gameObject);
    }

    public void moverBala()
    {
        transform.position = Vector3.MoveTowards(transform.position, firstPos, velocidad * Time.deltaTime);
        if (transform.position == firstPos)
            Destroy(gameObject);
    }
}
