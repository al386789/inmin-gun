using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertasManager : MonoBehaviour
{
    public Dictionary<string, GameObject[]> salas;
    public Dictionary<string, GameObject[]> enemigos;
    public bool[] estaEnSala;
    public GameObject[] enemigos1;
    public GameObject[] enemigos2;
    public GameObject[] enemigos3;
    public GameObject[] enemigos4;
    public GameObject[] enemigos5;


    public bool cerradas = false;


    // Start is called before the first frame update
    void Start()
    {
        estaEnSala = new bool[transform.childCount];
        for (int i = 0; i < estaEnSala.Length; i++)
        {
            estaEnSala[i] = false;
        }
        salas = new Dictionary<string, GameObject[]>();
        for (int i = 0; i < transform.childCount; i++)
        {
            GameObject[] puertas = new GameObject[transform.GetChild(i).childCount]; 
            for (int j = 0; j < transform.GetChild(i).childCount; j++)
            {
                puertas[j] = transform.GetChild(i).transform.GetChild(j).gameObject;
                
            }
            salas.Add(transform.GetChild(i).name, puertas);
        }

        enemigos.Add("sala1", enemigos1);
        enemigos.Add("sala2", enemigos2);
        enemigos.Add("sala3", enemigos3);
        enemigos.Add("sala4", enemigos4);
        estaEnSala = new bool[salas.Count];
    }

    public void EntrandoEnSala(int i)
    {
        estaEnSala[i] = true;
    }

    public void SaliendoSala(int i)
    {
        estaEnSala[i] = false;
    }

    private void Update()
    {
        for (int i = 0; i < estaEnSala.Length; i++)
        {
            if (estaEnSala[i])
            {
                
                if (!cerradas){
                    CerrarPuertas(salas[transform.GetChild(i).name]);
                }

                if (enemigos[transform.GetChild(i).name].Length == 0)
                    AbrirPuertas(salas[transform.GetChild(i).name]);
            }
                
        }
    }

    public void CerrarPuertas(GameObject[] puertas)
    {
        for (int i = 0; i < puertas.Length; i++)
        {
            Debug.Log(puertas[i]);
            puertas[i].SetActive(true);
        }
        cerradas = true;
    }

    public void AbrirPuertas(GameObject[] puertas) 
    {
        for (int i = 0; i < puertas.Length; i++)
        {
            puertas[i].SetActive(false);
        }
        cerradas = false;
    }
}
