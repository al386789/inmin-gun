using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemiesMovement : MonoBehaviour
{
    public GameObject player;
    public float vidaEnemigo;
    public int da�o;
    public float speed;
    public float reload;
    public int type; //0 ara�a 1 fantasma 2 pallaso 3 sapo
    public Transform[] wayPoints;
    public GameObject ataque;

    private int movementIndex;
    private Vector3 objective;
    private float time;
    private float startSpeed;

    private void Start()
    {
        movementIndex = 0;
        objective = transform.position;
        startSpeed = speed;
    }

    private void Update()
    {
        if( type == 1 || type == 3 || type == 2)
        {
            enemyMovement();
        }

        time += Time.deltaTime;
        if(time >= reload && (type == 0 || type == 1) )
        {
            time = 0;
            enemyAttack();
        }

        if(type == 2)
        {
            enemyAttack2();
        }

        if(type == 3)
        {
            enemyAttack3();
        }
    }

    private void enemyMovement()
    {
        if(objective == transform.position)
        {
            objective = wayPoints[movementIndex].position;
            movementIndex++;

            if (movementIndex == wayPoints.Length)
                movementIndex = 0;
        }

        transform.position = Vector3.MoveTowards(transform.position, objective, speed * Time.deltaTime);
    }

    private void enemyAttack()
    {
        var angle = Mathf.Atan2(player.transform.position.y, player.transform.position.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        if(Vector3.Distance(transform.position, player.transform.position) < 8)
            GetComponentInChildren<spawnBala>().CrearBala(ataque, player, da�o, false);
    }

    private void enemyAttack2()
    {
        var angle = Mathf.Atan2(player.transform.position.y, player.transform.position.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        
        if(Vector3.Distance(transform.position, player.transform.position) < 0.5)
        {
            player.GetComponent<personajeSalud>().QuitarVida(da�o);
        }

    }

    private void enemyAttack3()
    {
        var angle = Mathf.Atan2(player.transform.position.x, player.transform.position.y) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        if(Vector3.Distance(player.transform.position, transform.position) < 2)
            GetComponentInChildren<spawnBala>().CrearBala(ataque, player, da�o, false);
    }

    public void quitarVida()
    {
        vidaEnemigo -= player.GetComponent<personajeScript>().da�o;
        if (vidaEnemigo <= 0)
            Destroy(gameObject);

    }

    public void resetPowerUp()
    {
        speed = startSpeed;
    }
}
