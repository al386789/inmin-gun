using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class botonPuerta : MonoBehaviour
{
    public GameObject puerta;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Debug.Log("Abrir");
            puerta.GetComponent<BoxCollider2D>().enabled = false;
            puerta.GetComponent<SpriteRenderer>().enabled = true;
        }
    }
}
