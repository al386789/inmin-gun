using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlSalas : MonoBehaviour
{
    public PuertasManager puertasManager;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player" && !puertasManager.cerradas)
        {
            if (gameObject.name == "Sala1")
                puertasManager.EntrandoEnSala(0);
            else if (gameObject.name == "Sala2")
                puertasManager.EntrandoEnSala(1);
            else if (gameObject.name == "Sala3")
                puertasManager.EntrandoEnSala(2);
            else if (gameObject.name == "Sala4")
                puertasManager.EntrandoEnSala(3);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (gameObject.name == "Sala1")
            puertasManager.SaliendoSala(0);
        else if (gameObject.name == "Sala2")
            puertasManager.SaliendoSala(1);
        else if (gameObject.name == "Sala3")
            puertasManager.SaliendoSala(2);
        else if (gameObject.name == "Sala4")
            puertasManager.SaliendoSala(3);
    }
}
