using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drogas : MonoBehaviour
{
    public bool drogado = false;
    public string droga = "";
    public GameObject[] enemigos;
    public GameObject player;
    public Sprite altSprite;

    private float timer = 15.0f;
    [SerializeField]
    private float time = 0;

    private void Start()
    {
        if(FamilyFriendlyGame.EstadoFamilyFriendly())
        {
            GetComponent<SpriteRenderer>().sprite = altSprite;
        }
    }

    private void Update()
    {
        if (drogado)
        {
            time += Time.deltaTime;
            if (time >= timer)
            {
                time = 0;
                comprobarPowerUp();
            }
        }
    }

    public void comprobarPowerUp()
    {
        if (droga == "Porro")
        {
            for (int i = 0; i < enemigos.Length; i++)
            {
                enemigos[i].GetComponent<enemiesMovement>().resetPowerUp();
            }
        }
        else if(droga == "Speed")
        {
            player.GetComponent<personajeScript>().resetVariables(0);
        }
        else if(droga == "Jaco")
        {
            player.GetComponent<personajeScript>().resetVariables(1);
        }
        else if(droga == "DCanibal")
        {
            player.GetComponent<personajeSalud>().inmortal = false;
        }
        else if(droga == "Keta")
        {
            player.GetComponent<personajeScript>().balaDoble = false;
        }
        drogado = false;
        Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        
        if (col.gameObject == player)
        {
            drogado = true;
            if (droga == "Porro")
            {
                Debug.Log("Porro");
                
                for (int i = 0; i < enemigos.Length; i++)
                {
                    enemigos[i].GetComponent<enemiesMovement>().speed /= 2;
                }
            }
            else if (droga == "Speed")
            {
                Debug.Log("Speed");
                drogado = true;
                col.gameObject.GetComponent<personajeScript>().velocidad += col.gameObject.GetComponent<personajeScript>().velocidad / 2;

            }
            else if (droga == "Jaco")
            {
                Debug.Log("Jaco");
                col.gameObject.GetComponent<personajeScript>().da�o += col.gameObject.GetComponent<personajeScript>().da�o/3;
            }
            else if (droga == "DCanival")
            {
                Debug.Log("DCanival");
                col.gameObject.GetComponent<personajeSalud>().inmortal = true;
            }
            else if (droga == "Keta")
            {
                Debug.Log("Keta");
               col.gameObject.GetComponent<personajeScript>().balaDoble = true;
            }
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
        }
    }
}
